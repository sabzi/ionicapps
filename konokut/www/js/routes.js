angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


  .state('tabs', {
    url: '/tabs',
    templateUrl: 'templates/tabs.html',
    controller: 'tabsController'
  })


      .state('tabs.activity', {
    url: '/activity',
    views: {
      'activity': {
        templateUrl: 'templates/activity.html',
        controller: 'activityCtrl'
      }
    }
  })

  .state('tabs.waves', {
    url: '/waves',
    views: {
      'waves': {
        templateUrl: 'templates/waves.html',
        controller: 'wavesCtrl'
      }
    }
  })

  .state('tabs.contacts', {
    url: '/contacts',
    views: {
      'contacts': {
        templateUrl: 'templates/contacts.html',
        controller: 'contactsCtrl'
      }
    }
  })


//TODO change routes to wave.{subview}
  .state('wavechat', {
    url: '/wave/:id/chat',
    templateUrl: 'templates/wavechat.html',
    controller: 'wavechatCtrl'
  })
  .state('wavecat', {
    url: '/wave/:id/cat',
    templateUrl: 'templates/wavecat.html',
    controller: 'wavecatCtrl'
  })
  .state('wavetasks', {
    url: '/wave/:id/:cat/tasks',
    templateUrl: 'templates/wavetasks.html',
    controller: 'wavetasksCtrl'
  })

$urlRouterProvider.otherwise('/tabs/activity')



});
