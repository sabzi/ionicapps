angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])
.service('BlankService', [function(){

}])
.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Abc paid $10 to you',
    lastText: 'Trip to Goa',
    face: 'img/ben.png',
    time: '5:55 PM',
    date:"2014-04-29T02:52:47.706Z",
    user: {id:1,name:"konokut"}
  }, {
    id: 1,
    name: 'Xyz paid $25 - Balloons',
    lastText: 'Birthday celebration',
    face: 'img/max.png',
    time: '3:33 PM',
    date:"2014-04-29T02:52:47.706Z",
    user: {id:2,name:"max"}
  }, {
    id: 2,
    name: 'Cbz assigned \'UI Design\' to you',
    lastText: 'Ab project work',
    face: 'img/adam.jpg',
    time: '11:49 AM',
    date:"2014-04-29T02:52:47.706Z",
    user: {id:3,name:"adam"}
  }, {
    id: 3,
    name: 'James added \'Paragliding\'',
    lastText: 'Trip to Goa',
    face: 'img/perry.png',
    time: 'Yesterday',
    date:"2014-04-29T02:52:47.706Z",
    user: {id:1,name:"konokut"}
  }, {
    id: 4,
    name: 'Mike added \'Be there by 5PM\'',
    lastText: 'Birthday celebration',
    face: 'img/mike.png',
    time: 'Yesterday',
      date:"2014-04-29T02:52:47.706Z",
    user: {id:3,name:"adam"}
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('WaveChats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var wavechats = [{
    id: 0,
    text: 'Hi \n Trip to Goa',
    date:"2016-07-29T02:52:47.706Z",
    user: {id:1,name:"konokut",face: 'img/konokut.png'}
  }, {
    id: 1,
    text: 'I\'m in :)',
    date:"2016-07-29T02:58:47.706Z",
    user: {id:2,name:"max",face: 'img/max.png'}
  }, {
    id: 2,
    text: 'Book 6 tickets',
    date:"2016-07-29T02:59:47.706Z",
    user: {id:1,name:"konokut",face: 'img/konokut.png'}
  }, {
    id: 3,
    text: 'I will check options in \n makemytrip',
    date:"2016-07-29T03:11:47.706Z",
    user: {id:3,name:"mike",face: 'img/mike.png'}
  }, {
    id: 4,
    text: 'I have a coupon code \n let me send u',
    date:"2016-07-29T03:15:47.706Z",
    user: {id:2,name:"max",face: 'img/max.png'}
  }];

  return {
    all: function() {
      return wavechats;
    },
    remove: function(wavechat) {
      wavechats.splice(wavechats.indexOf(wavechat), 1);
    },
    add: function(wavechat){
      wavechats.push(wavechat);
    },
    get: function(chatId) {
      for (var i = 0; i < wavechats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return wavechats[i];
        }
      }
      return null;
    }
  };
})


.factory('Waves', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var waves = [{
    id: 0,
    name: 'Trip to Goa',
    lastText: 'A: Got to start early',
    face: 'img/trip.jpg',
    time: '5:55 PM',
    messages: 3
  }, {
    id: 1,
    name: 'Abc project work',
    lastText: 'D: Testing in progress',
    face: 'img/project.jpg',
    time: '3:33 PM'
  }, {
    id: 2,
    name: 'Steve birthday party',
    lastText: 'R: Buy cake',
    face: 'img/party.jpg',
    time: '11:49 AM'
  }];

  return {
    all: function() {
      return waves;
    },
    remove: function(wave) {
      waves.splice(waves.indexOf(wave), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < waves.length; i++) {
        if (waves[i].id === parseInt(chatId)) {
          return waves[i];
        }
      }
      return null;
    }
  };
})
.factory('Categories', function() {
  var categories = [{
                      name:"Exepences",
                      face:"img/exepences.png",
                      tasks:[
                            {name:"Purchased tickes",righticon:"ion-social-usd",lefticon:"ion-android-plane"},
                            {name:"Booked hotel",righticon:"ion-card",lefticon:"ion-android-restaurant"},
                            {name:"Booked Cab",righticon:"ion-card",lefticon:"ion-model-s"}
                          ]
                    },
                    {
                      name:"Booking",
                      face:"img/booking.png",
                      tasks:[
                            {name:"Booked hotel",righticon:"ion-card",lefticon:"ion-android-restaurant"},
                            {name:"Booked Cab",righticon:"ion-card",lefticon:"ion-model-s"}
                          ]
                      },
                      {
                        name:"Places",
                        face:"img/places.png",
                        tasks:[
                              {name:"Goa palace",righticon:"ion-images",lefticon:"ion-image"},
                              {name:"Beach",righticon:"ion-images",lefticon:"ion-image"},
                              {name:"Fort",righticon:"ion-images",lefticon:"ion-image"}
                            ]
                    } ];
  return {
    all: function() {
      return categories;
    },
    get: function(catname) {
      for (var i = 0; i < categories.length; i++) {
        if (categories[i].name === catname) {
          return categories[i];
        }
      }
      return null;
    }
  };
});
