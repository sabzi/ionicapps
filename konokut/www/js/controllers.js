angular.module('app.controllers', [])

.controller('tabsController',  // TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$rootScope) {
  $scope.user =$rootScope.user;
})
.controller('activityCtrl',// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, Chats) {
  $scope.chats = Chats.all();
    $scope.remove = function(chat) {
      Chats.remove(chat);
    };
})

.controller('wavesCtrl', function($scope, Waves) {
  $scope.waves = Waves.all();
  $scope.remove = function(wave) {
    Waves.remove(wave);
  };
})

.controller('contactsCtrl', ['$scope', '$stateParams', // TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('wavechatCtrl',  // TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$ionicHistory,$rootScope, Waves, WaveChats) {
  $scope.wave = Waves.get($stateParams.id);
  $scope.chats = WaveChats.all();
  $scope.inputmessage="";
  $scope.onSwipeDown = function(){

  };
  $scope.sendMessage = function(sendMessageForm) {
      var message = {
        id:   $scope.chats.length+1,
        text: $scope.inputmessage
      };
      $scope.inputmessage = '';

      message.user = $rootScope.user
      message.date = new Date();
    //  $scope.chats.push(message);
      WaveChats.add(message);
      scrollToBottom();
    };

})
.controller('wavecatCtrl',  // TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$ionicHistory,$rootScope, Waves, Categories) {
  $scope.wave = Waves.get($stateParams.id);
  $scope.categories = Categories.all();
})
.controller('wavetasksCtrl',  // TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$ionicHistory,$rootScope, Waves, Categories) {
  $scope.wave = Waves.get($stateParams.id);
  $scope.cat = Categories.get($stateParams.cat);
  $scope.tasks = $scope.cat.tasks;
})
