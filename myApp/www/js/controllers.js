angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.username ="maruthi";

  $scope.menulist = [
    { title: 'Home', path: "home",icon:"ion-ios-home",state:"home" },
    { title: 'Groups', path: "groups",icon:"ion-ios-people",state:"groups" },
    { title: 'Settle', path: "settle",icon:"ion-social-usd",state:"settle" },
    { title: 'Search', path: "search",icon:"ion-android-search",state:"search"},
    { title: 'MyTask', path: "mytasks",icon:"ion-checkmark-circled" ,state:"mytasks"},
    { title: 'Trash', path: "trash",icon:"ion-ios-trash",state:"trash" },
    { title: 'Settings', path: "settings",icon:"ion-gear-b",state:"settings" }
  ];

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('TasksCtrl', function($scope,$ionicModal) {
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.taskForm = modal;
    });

  $scope.tasklist = [
    { title: 'My Task One', id: 1, subtask:2 },
    { title: 'My Task Two', id: 2, subtask:0  },
    { title: 'My Task Three', id: 3 , subtask:3 },
  ];

  $scope.showTaskForm = function(){
    console.log("showform");
    $scope.taskForm.show();
  };
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
